<?php
error_reporting(E_ALL);

require_once("settings.php");

ini_set('display_errors', true);

date_default_timezone_set('Europe/Berlin');

create_graph($folder . "login-minute.gif", -900, "15 Minute", "60");
create_graph($folder . "login-hour.gif", -3600, "Hourly", "60");
create_graph($folder . "login-day.gif", -3600 * 24, "Daily", "3600");
create_graph($folder . "login-week.gif", -3600 * 24 * 7, "Weekly", "3600");
create_graph($folder . "login-month.gif", -3600 * 24 * 30, "Monthly", "86400");
create_graph($folder . "login-year.gif", -3600 * 24 * 365, "Yearly", "86400");

//getlast();

echo "<table>";
echo "<tr><td>";
echo "<img src='" . $folder . "login-minute.gif' alt='Generated RRD image'>";
echo "</td></tr>";
echo "<tr><td>";
echo "<img src='" . $folder . "login-hour.gif' alt='Generated RRD image'>";
echo "</td></tr>";
echo "<tr><td>";
echo "<img src='" . $folder . "login-day.gif' alt='Generated RRD image'>";
echo "</td><td>";
echo "<img src='" . $folder . "login-week.gif' alt='Generated RRD image'>";
echo "</td></tr>";
echo "<tr><td>";
echo "<img src='" . $folder . "login-month.gif' alt='Generated RRD image'>";
echo "</td><td>";
echo "<img src='" . $folder . "login-year.gif' alt='Generated RRD image'>";
echo "</td></tr>";
echo "</table>";
exit;

function getlast(){
    global $rrdfile, $impperkwh;
    $fact = 1800 * 2000 / $impperkwh;

    
     $options = array(
    "--slope-mode",
    "--start", "-1d",
    "--vertical-label=User login attempts",
    "--lower=0",
    "DEF:day=$rrdfile:energyday:AVERAGE",
    "DEF:night=$rrdfile:energynight:AVERAGE",
    "CDEF:tday=day,$fact,*",
    "CDEF:tnight=night,$fact,*",
    "CDEF:ttotal=tday,tnight,+",
    "VDEF:current=ttotal,LAST"
  );
  
    $result = rrdgraph_data( $rrdfile, $options );
    echo print_r($result);
}

function create_graph($output, $intervall, $title, $step) {
    global $impperkwh, $costday, $costnight, $rrdfile;
    $fac = 1800 * 2000 / $impperkwh;
    $totalfactor = $intervall / 3600.0 / 1000 * -1;
  $options = array(
    "--slope-mode",
    "--start", $intervall,
    "--title=$title",
    "--vertical-label=Watt",
    "--lower=0",
    "--step=$step",
    "DEF:day=$rrdfile:energyday:AVERAGE",
    "DEF:night=$rrdfile:energynight:AVERAGE",
    "CDEF:tday=day,$fac,*",
    "CDEF:tnight=night,$fac,*",
    "CDEF:ttotal=tday,tnight,+",
    "VDEF:current=ttotal,LAST",
    "CDEF:tdaytotal=tday,UN,0,tday,$totalfactor,*,IF",
    "CDEF:tnighttotal=tnight,UN,0,tnight,$totalfactor,*,IF",
    "CDEF:ttotaltotal=ttotal,UN,0,ttotal,$totalfactor,*,IF",
    "CDEF:cdaytotal=tdaytotal,$costday,*",
    "CDEF:cnighttotal=tnighttotal,$costnight,*",
    "CDEF:ctotaltotal=cnighttotal,cdaytotal,+",
    "AREA:tday#00FF00:Tagstrom",
    "STACK:tnight#FF0000:Nachstrom",
   // "COMMENT: \\n",
    //"GPRINT:current:Aktuell   %6.0lf W",
    "COMMENT: \\n",
    "GPRINT:tday:AVERAGE:Tagstrom  %6.0lf W",
    "GPRINT:tdaytotal:AVERAGE: %6.2lf kWh",
    "GPRINT:cdaytotal:AVERAGE: %4.2lf €",
    "COMMENT: \\n",
    "GPRINT:tnight:AVERAGE:Nachstrom %6.0lf W",
    "GPRINT:tnighttotal:AVERAGE: %6.2lf kWh",
    "GPRINT:cnighttotal:AVERAGE: %4.2lf €",
    "COMMENT: \\n",
    "GPRINT:ttotal:AVERAGE:Total     %6.0lf W",
    "GPRINT:ttotaltotal:AVERAGE: %6.2lf kWh",
    "GPRINT:ctotaltotal:AVERAGE: %4.2lf €",
    "COMMENT: \\n"
  );

  $ret = rrd_graph($output, $options);
  if (! $ret) {
    echo "<b>Graph error: </b>".rrd_error()."\n";
  }
}

?>